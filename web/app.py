from flask import Flask, render_template, request
import os
app = Flask(__name__)

@app.route("/")
def hello():
    name = request.args.get('name')
    if name == None:
        return "UOCIS docker demo!"


@app.route('/<blrb>')
def transmit(blrb):
#    print(blrb)

    if blrb.endswith('.html'):
        blrb = blrb[:-5]

#    print(blrb)
    if "//" in blrb or '..' in blrb or '~' in blrb:
        return error_403(403)
    else:
        try:
            return render_template('{}.html'.format(blrb))
        except IOError:
            return error_404(404)

@app.errorhandler(404)
def error_404(error):
    return render_template('404.html'), 404

@app.errorhandler(403)
def error_403(error):
    return render_template('403.html'), 403

if __name__ == "__main__":
    app.run(debug=True,host='0.0.0.0')
