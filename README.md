THis is a barebones flask program that can transmit the "trivia.html" template
with the appropriate css.  If an invalid or not found url is entered, a 403
or 404 error header and error html will be transmited respectively.  The 
initial web page (no url extension) returns the given string "UO Cis Demo"
